package com.sda.tutorials.demoRealProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRealProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRealProjectApplication.class, args);
	}

}
